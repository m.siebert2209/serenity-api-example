## Serenity Api Example

### Description
Following repository contains simple tests written in Java and [Serenity](https://serenity-bdd.github.io/),
 in order to test critics and reviews endpoints of 
 [Moview Reviews API](https://developer.nytimes.com/docs/movie-reviews-api/1/overview) 

### Project structure
General tests files can be found under `/src/test/java/moviereviewsapi/` path.

Tests scenarios are placed under `src/test/resources/features/` path. In order to add new test just create
new gherkin scenario and generate steps into stepdefinitions folder8 and start implementation.
 
### How to run tests
Before running tests locally please generate new or use existing api key from [NY Times developer site](https://developer.nytimes.com/get-started) 
and paste it into serenity.conf file:

```
restapi {
 baseurl = "https://api.nytimes.com/svc/movies/v2"
 token = ""
}
```
 
In order to run all tests just type following commands using Maven:

`mvn clean verify` - with token added in serenity.conf

`mvn clean verify -Drestapi.token=API_TOKEN` - for maven run 

You can also run tags by cucumber tags:

`mvn clean verify --Dcucumber.options="--tags @color=reviews`

### CI/CD

Test results are published under built-int gitlab page:
https://m.siebert2209.gitlab.io/serenity-api-example/
Results are published only after pipeline run on master branch.
