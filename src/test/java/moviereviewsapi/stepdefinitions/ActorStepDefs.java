package moviereviewsapi.stepdefinitions;

import io.cucumber.java.en.Given;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import java.util.Optional;

public class ActorStepDefs {

    private EnvironmentVariables environmentVariables;

    @Given("^that (.*) is authenticated$")
    public void thatActorIsAuthenticated(String actorName) {
        String movieApiBaseUrl = getValueFor("restapi.baseurl");
        String token = getValueFor("restapi.token");

        Actor parent = Actor.named(actorName).whoCan(CallAnApi.at(movieApiBaseUrl));
        Serenity.setSessionVariable(Actor.class).to(parent);
        Serenity.setSessionVariable("token").to(token);
    }

    private String getValueFor(String propertyName) {
        return Optional.ofNullable(System.getProperty(propertyName))
                .orElse(environmentVariables.getProperty(propertyName));
    }
}
