package moviereviewsapi.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import moviereviewsapi.models.critic.Critics;
import moviereviewsapi.tasks.ActorTasks;
import moviereviewsapi.tasks.EmployerTasks;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class CriticsStepDefs {

    private final Actor employer = Serenity.sessionVariableCalled(Actor.class);
    private final String token = Serenity.sessionVariableCalled("token");

    @When("^Employer checks specific (.*)$")
    public void iCheckSpecificCritic(String criticName) {
        employer.attemptsTo(EmployerTasks.fetchCriticData(criticName, token));

        assertThat(ActorTasks.getLastResponse().statusCode()).isEqualTo(200);
    }

    @Then("^Employer should see that (.*) does not exist$")
    public void iCanSeeThatCriticDoesNotExist(String criticName) {
        Critics critic = ActorTasks.convertResponseToObject(Critics.class);

        assertThat(critic.getResults()).isNullOrEmpty();
    }

    @Then("^Employer should see that (.*) employment status is (.*)$")
    public void employerShouldSeeThatCriticEmploymentStatusIsStatus(String criticName, String statusName) {
        Critics critic = ActorTasks.convertResponseToObject(Critics.class);

        assertThat(critic.getStatus()).isEqualTo("OK");
        assertThat(critic.getResults()[0].getStatus()).isEqualTo(statusName);
    }
}
