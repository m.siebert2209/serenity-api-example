package moviereviewsapi.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import moviereviewsapi.enums.MpaRating;
import moviereviewsapi.models.review.Reviews;
import moviereviewsapi.tasks.ActorTasks;
import moviereviewsapi.tasks.ParentTasks;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class MoviesReviewsStepDefs {

    private final Actor parent = Serenity.sessionVariableCalled(Actor.class);
    private final String token = Serenity.sessionVariableCalled("token");

    @When("^Parent opens (.*) review$")
    public void parentOpensMovieReview(String movieName) {
        parent.attemptsTo(ParentTasks.fetchReviewsData(movieName, token));

        assertThat(ActorTasks.getLastResponse().statusCode()).isEqualTo(200);
    }

    @Then("^Parent should see that MPA rating of that movie should be (.*)$")
    public void parentShouldSeeThatMPARatingOfThatMovieShouldBeRating(String mpaRating) {
        Reviews critic = ActorTasks.convertResponseToObject(Reviews.class);

        assertThat(critic.getStatus()).isEqualTo("OK");
        assertThat(critic.getResults()[0].getMpaa_rating()).isEqualTo(MpaRating.of(mpaRating));
    }
}
