package moviereviewsapi.models.review;

public class ReviewsResults {

    private String displayTitle;
    private String mpaa_rating;
    private String critics_pick;
    private String byline;
    private String headline;

    public String getDisplayTitle() {
        return displayTitle;
    }

    public String getMpaa_rating() {
        return mpaa_rating;
    }

    public String getCritics_pick() {
        return critics_pick;
    }

    public String getByline() {
        return byline;
    }

    public String getHeadline() {
        return headline;
    }
}
