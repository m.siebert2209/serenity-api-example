package moviereviewsapi.models.review;

public class Reviews {

    private String status;
    private String copyright;
    private ReviewsResults[] results;

    public String getStatus() {
        return status;
    }

    public String getCopyright() {
        return copyright;
    }

    public ReviewsResults[] getResults() {
        return results;
    }
}
