package moviereviewsapi.models.critic;

public class Critics {

    private String status;
    private String copyright;
    private CriticResults[] results;

    public String getStatus() {
        return status;
    }

    public String getCopyright() {
        return copyright;
    }

    public CriticResults[] getResults() {
        return results;
    }
}
