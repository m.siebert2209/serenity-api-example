package moviereviewsapi.models.critic;

public class CriticResults {

    private String displayName;
    private String sortName;
    private String status;
    private String bio;
    private String seoName;

    public String getDisplayName() {
        return displayName;
    }

    public String getSortName() {
        return sortName;
    }

    public String getStatus() {
        return status;
    }

    public String getBio() {
        return bio;
    }

    public String getSeoName() {
        return seoName;
    }
}
