package moviereviewsapi.enums;

import java.util.Arrays;

public enum MpaRating {

    G("general audiences"),
    PG("parental guidance suggested"),
    PG13("parents strongly cautioned"),
    R("restricted");

    private final String ratingMeaning;

    MpaRating(String ratingMeaning) {
        this.ratingMeaning = ratingMeaning;
    }

    public static String of(String rating) {
        return Arrays.stream(values())
                .filter(value -> value.ratingMeaning.equals(rating))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Incorrect rating provided"))
                .toString();
    }
}
