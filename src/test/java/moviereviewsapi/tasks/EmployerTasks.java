package moviereviewsapi.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class EmployerTasks {

    public static Task fetchCriticData(String criticName, String token) {
        return Task.where("{0} fetches critic data",
                Get.resource("/critics/{reviewer}.json?api-key=" + token)
                        .with(request -> request.pathParam("reviewer", criticName)));
    }
}

