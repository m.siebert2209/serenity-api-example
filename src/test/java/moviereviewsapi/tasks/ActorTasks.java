package moviereviewsapi.tasks;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class ActorTasks {

    public static <T> T convertResponseToObject(Class<T> classType) {
        return getLastResponse().jsonPath().getObject("", classType);
    }

    public static Response getLastResponse() {
        return SerenityRest.lastResponse();
    }
}