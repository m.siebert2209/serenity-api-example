package moviereviewsapi.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class ParentTasks {

    public static Task fetchReviewsData(String movieName, String token) {
        return Task.where("{0} opens movie review",
                Get.resource("/reviews/search.json?critics-pick=y&query=" + movieName + "&api-key=" + token));
    }
}

