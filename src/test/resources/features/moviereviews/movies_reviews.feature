@reviews
Feature: Movies reviews

  Scenario Outline: Check if movie has proper MPA rating
    Given that parent is authenticated
    When Parent opens <movie> review
    Then Parent should see that MPA rating of that movie should be <rating>

    Examples:
      | movie            | rating     |
      | The Big Lebowski | restricted |
      | The Godfather    | restricted |
