@critics
Feature: Critics

  Background: Setup endpoint connection
    Given that employer is authenticated

  Scenario Outline: Check critics employment status
    When Employer checks specific <critic>
    Then Employer should see that <critic> employment status is <status>

    Examples:
      | critic          | status    |
      | Anita Gates     | part-time |
      | Neil Genzlinger | part-time |


  Scenario Outline: Check critics names - negative scenario
    When Employer checks specific <critic>
    Then Employer should see that <critic> does not exist

    Examples:
      | critic       |
      | Jan Kowalski |
      | John Doe     |

